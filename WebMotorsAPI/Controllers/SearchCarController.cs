﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebMotorsAPI.Models;
using WebMotorsAPI.Services.SearchCar.Interfaces;

namespace WebMotorsAPI.Controllers
{
    [Route("api/v1/[Controller]")]
    [ApiController]
    public class SearchCarController : ControllerBase
    {
        private readonly ISearchCarService _searchCarService;

        public SearchCarController(ISearchCarService searchCarService)
        {
            _searchCarService = searchCarService;
        }

        [HttpGet("cars")]
        public async Task<IEnumerable<Car>> GetCar()
        {
            return await _searchCarService.GetCarAsync();
        }

        [HttpGet("cars/{id}")]
        public async Task<ActionResult<Car>> GetCarById(int id)
        {
            var car = await _searchCarService.GetCarByIdAsync(id);
            if (car == null)
            {
                return NotFound();
            }
            return car;
        }

        [HttpGet("cars/year/{year}")]
        public async Task<IEnumerable<Car>> GetCarByYear(int year)
        {
            return await _searchCarService.GetCarByYearAsync(year);
        }

        [HttpGet("makes")]
        public async Task<IEnumerable<Make>> GetMake()
        {
            return await _searchCarService.GetMakeAsync();
        }

        [HttpGet("makes/{id}")]
        public async Task<ActionResult<Make>> GetMakeById(int id)
        {
            var make = await _searchCarService.GetMakeByIdAsync(id);
            if (make == null)
            {
                return NotFound();
            }
            return make;
        }

        [HttpGet("makes/{id}/cars")]
        public async Task<IEnumerable<Car>> GetCarsByMakeId(int id)
        {
            return await _searchCarService.GetCarsByMakeIdAsync(id);
        }
    }
}
