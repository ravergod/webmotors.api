﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebMotorsAPI.Models;

namespace WebMotorsAPI.Services.SearchCar.Interfaces
{
    public interface ISearchCarService
    {
        Task<IEnumerable<Car>> GetCarAsync();
        Task<Car> GetCarByIdAsync(int id);
        Task<IEnumerable<Make>> GetMakeAsync();
        Task<Make> GetMakeByIdAsync(int id);
        Task<IEnumerable<Car>> GetCarByYearAsync(int year);
        Task<IEnumerable<Car>> GetCarsByMakeIdAsync(int id);
    }
}
