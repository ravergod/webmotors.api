﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMotorsAPI.Context;
using WebMotorsAPI.Models;
using WebMotorsAPI.Services.SearchCar.Interfaces;

namespace WebMotorsAPI.Services.SearchCar
{
    public class SearchCarService : ISearchCarService
    {
        private readonly AppDbContext _context;
        public SearchCarService(AppDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Car>> GetCarAsync()
        {
            return await _context.Cars.ToListAsync();
        }

        public async Task<Car> GetCarByIdAsync(int id)
        {
            return await _context.Cars.AsNoTracking().Include(x => x.Make).FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Make>> GetMakeAsync()
        {
            return await _context.Makes.Include(x => x.Cars).ToListAsync();
        }

        public async Task<Make> GetMakeByIdAsync(int id)
        {
            return await _context.Makes.AsNoTracking().Include(x => x.Cars).FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<Car>> GetCarByYearAsync(int year)
        {
            var stringYear = year.ToString();
            return await _context.Cars.Where(c => c.Year == stringYear).ToListAsync();
        }

        public async Task<IEnumerable<Car>> GetCarsByMakeIdAsync(int id)
        {
            return await _context.Cars.Where(c => c.MakeId == id).ToListAsync();
        }
    }
}
