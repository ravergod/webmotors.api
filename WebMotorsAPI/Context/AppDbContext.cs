﻿using Microsoft.EntityFrameworkCore;
using WebMotorsAPI.Models;

namespace WebMotorsAPI.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }
        public DbSet<Make> Makes { get; set; }
        public DbSet<Car> Cars { get; set; }
    }
}
