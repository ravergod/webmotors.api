﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebMotorsAPI.Models
{
    [Table("Makes")]
    public class Make
    {
        public Make()
        {
            Cars = new Collection<Car>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(45)]
        public string Name { get; set; }

        public ICollection<Car> Cars { get; set; }
    }
}
