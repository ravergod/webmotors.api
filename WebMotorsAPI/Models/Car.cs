﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebMotorsAPI.Models
{
    [Table("Cars")]
    public class Car
    {
        [Key]
        public int Id { get; set; }
        public Make Make { get; set; }
        public int MakeId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Model { get; set; }
        [Required]
        [MaxLength(4)]
        public string Year { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
    }
}
